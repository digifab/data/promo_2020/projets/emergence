#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from numpy.random import randint, choice
from datetime import date, timedelta
from numpy.random import randint, choice
from datetime import date, timedelta
import datetime
import sqlite3 as lite
import pandas as pd
import os.path
import schedule
import networkx as nx
import time

import smtplib
from pathlib import Path

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders

### ACTIVATION DES TESTS 
TEST = True 
mail="MONMAIL"
password="MONMOTDEPASSE"
# Pour chaque table, on donne les noms des colonnes, ainsi que le type de 
# données au format SQL 
STRUCTURE_BASE = {
    'connaissances': 
        {'id_connaissance': 'INT', 
         'pere': 'TEXT',
         'nom': 'TEXT',
         'rang': 'INT'},
    'evaluations':
        {'id_evaluation': 'INT',
         'note': 'INT',
         'evaluateur': 'TEXT',
         'date_note': 'DATE',
         'id_connaissance': 'INT',
         'id_eleve': 'INT'},
    'eleves':
        {'id_eleve': 'INT',
         'nom': 'TEXT',
         'prenom': 'TEXT',
         'date_de_naissance': 'DATE',
         'telephone': 'TEXT',
         'mail': 'TEXT',
         'description': 'TEXT'},
    'authentification':
        {'id_auth': 'INT',
         'niveau_de_compte': 'INT',
         'nom_de_compte': 'TEXT',
         'mail': 'TEXT',
         'mot_de_passe': 'TEXT'}}
                 
def generation_donnees():
    """ Pour concevoir les programmes, on crée un exemple minimal de données
    sous forme de dictionnaires et de listes. Certaines données seront
    renseignées aléatoirement.  
    """
    # Ensembles des élèves et des formateurs
    eleves = ['Peggy', 'Mathieu', 'Sabrina', 'Pierre', 'Reda', 'Cécile',
              'Alexis', 'Lee Roy', 'Siham', 'JBG', 'JBT', 'Nicolas', 'Nico',
              'Léa', 'Seb', 'Florian', 'Andy', 'Joseph']
    
    formateurs = ['Vincent', 'Alexandre']
    
    # Ensemble des mots clefs associés à chaque connaissance élémentaires que
    # l'on pourra noter. Ces mots clefs permettront de réaliser une 'cascade'
    # de sous-groupes et d'obtenir ainsi une vision arborescente des
    # connaissances.
    connaissances = {
         'CSS': ('web',),
         'flask': ('web',),
         'gestion des erreurs': ('python',),
         'requêtes SQL': ('base de données', 'SQL'),
         'jinja': ('web', 'HTML'),
         'pandas': ('python', 'librairie', 'calcul matriciel'),
         'numpy': ('python', 'librairie', 'calcul matriciel'),
         'matplotlib': ('python', 'librairie', 'graphiques'),
         'for,while': ('python', 'bases', 'récursivité'),
         'liste': ('python', 'bases', 'structure de données'),
         'dictionnaire': ('python', 'bases', 'structure de données'),
         'string': ('python', 'bases', 'chaînes de caractères'),
         'Scrum': ('organisation', 'méthode Agile', 'théorie'),
         'Extrême Programming': ('organisation', 'méthode Agile', 'théorie'),
         'FDD': ('organisation', 'méthode Agile', 'théorie'),
         'Scrum Master': ('organisation', 'méthode Agile', 'capacité à être'),
         'Product Owner': ('organisation', 'méthode Agile', 'capacité à être')}
    
    # Ensemble des évaluations pour chaque connaissance élémentaire. A chaque
    # note est associée une connaissance, une date de notation, un élève noté,
    # ainsi qu'un évaluateur sous cette forme de dictionaire: 
    # evaluation = {
    #     'note': 9,
    #     'connaissance': 'requêtes SQL',
    #     'élève': 'Sabrina',
    #     'date': '01/04/2020',
    #     'évaluateur':'Vincent'}
    evaluations = []
    x = 1
    for eleve in eleves:
        for connaissance in connaissances:
            for debut_periode in [date(2020,4,1),
                                  date(2020,6,1),
                                  date(2020,8,1),
                                  date(2020,10,1),
                                  date(2020,11,1)]:
                evaluation = {}
                evaluation['id'] = x
                
                #nombre random entre 0 et 10+1
                evaluation['note'] = randint(0,11)
                evaluation['connaissance'] = connaissance
                evaluation['élève'] = eleve
                
                # Une durée qui exprime la différence entre deux instances 
                # de date, time ou datetime en microsecondes.
                # timedelta = durée entre deux dates, possibilité de faire 
                # addition ou soustraction de dates
                evaluation['date'] = debut_periode + timedelta(randint(0,61))
                
                #p = probabilité ?
                evaluation['évaluateur'] = choice(formateurs, p=[.3, .7])
                evaluations.append(evaluation)
        x+=1
        print(evaluations)
    return eleves, formateurs, connaissances, evaluations


def initialisation_base_de_donnees(mode='fictif', fichier=None,
                                   nom_base='test.db'):
    """ Cette fonction initialise la base de données à l'aide de la
    bibliothèque python sqlite. L'initialisation de ces données sera à l'image
    de ce qui sera créée au fil du temps par les utilisateurs le l'application
    web de visualisation des connaissances. On se donne trois modes
    d'initialisation qui répondront à l'ensemble de nos besoins: 
        1 - 'vierge': La base est vide de données, mais les tables, les noms
    des champs de chaque table, ainsi que le type des données que ces champs
    vont contenir sont déjà créés. Seule la structure de la base est ainsi mis
    en place. On veillera à détruire les anciennes données s'il en existe.
        2 - 'fictif': Structure de la base est constrtuire, puis elle est
    renseignée à l'aide de données fictives générées aléatoirement.
        3 - 'rechargement': A l'aide de fichiers de sauvegarde la base de
    données est rechargée. 
    
    La page suivante permet de comprendre comment utiliser sqlite:
    http://zetcode.com/db/sqlitepythontutorial/ """
 
    def rechargement(nom_base, fichier):
        # connexion à la bdd
        con = lite.connect(nom_base)
        xl = pd.ExcelFile(fichier)
        
        # pour tous les noms de tables récupérés par xl.sheet_names
        for nom_table, i in zip(xl.sheet_names, range(len(xl.sheet_names))):
            # data frame du fichier excel
            df = pd.read_excel(fichier, sheet_name = i, header=0, 
                               index_col=0, keep_default_na=True)
            # on récupère les données de la df pour transfert en sql
            df.to_sql(nom_table, con, if_exists = 'replace')
            
        # fermeture de la connexion
        con.close()
        
    
    if mode == 'fictif':
        # Création des des tables à partir d'un fichier Excel 
        rechargement(nom_base, fichier)
        
        # La table des évaluations est écrasée avec des évaluations fictives  
        evaluations = generation_donnees()[3]
        con = lite.connect(nom_base)
        with con:
            cur = con.cursor()
            cur.execute("DROP TABLE IF EXISTS evaluations") 
            requete_sql  = "CREATE TABLE evaluations"
            requete_sql += "(id INT, note INT, "
            requete_sql += "connaissance TEXT, eleve TEXT, date DATE, "
            requete_sql += "evaluateur TEXT);"
            cur.execute(requete_sql)
            for evaluation in evaluations:
                annee = str(evaluation["date"])[0:4]
                mois  = str(evaluation["date"])[5:7]
                jour  = str(evaluation["date"])[8:10]
                requete_sql  = "INSERT INTO evaluations VALUES("
                requete_sql += str(evaluation["id"])+","
                requete_sql += str(evaluation["note"])+","
                requete_sql += "'"+evaluation["connaissance"]+"',"
                requete_sql += "'"+evaluation["élève"]+"',"
                requete_sql += "'"+annee+'-'+mois+'-'+jour+"',"
                requete_sql += "'"+evaluation["évaluateur"]+"')"
                cur.execute(requete_sql) 
            if TEST:
                print(30*'-')
                print(requete_sql)
        cur.close()
        
                    
    elif mode == 'vierge':
        #requêtes SQL pour créer des tables vides 
        con = lite.connect(nom_base)
        with con:
            cur = con.cursor()
    
            for table, colonnes in STRUCTURE_BASE.items():
                requete_sql = "CREATE TABLE IF NOT EXISTS "+table+" ( "
                for colonne, type_sql in colonnes.items():
                    requete_sql += colonne +" "+type_sql+", "
                requete_sql = requete_sql[:-2]+")"
                cur.execute(requete_sql)
                
                    
    elif mode == 'rechargement':
        rechargement(nom_base, fichier)

def acquisition_donnees(nom_base):
    #Connection et requete a la base de données
    
    con = lite.connect(nom_base)
    with con:
        cur=con.cursor()
        cur.execute("select * from evaluations")
        resultat = cur.fetchall()      
        #Remplissage de la liste de la liste de dictionnaires evaluations
        #grâce aux données
        evaluations = []  
        for i in range(0, len(resultat)):
            ligne = resultat[i]
            evaluation = {}
            evaluation['id'] = ligne[0]
            evaluation['note'] = ligne[1]
            evaluation['connaissance'] = ligne[2]
            evaluation['élève'] = ligne[3]        
            date_str = ligne[4]
            annee = int(date_str[0:4])
            print(30*"*", date_str)
            mois = int(date_str[5:7])
            jour = int(date_str[8:10])
            evaluation['date'] = date(annee,mois,jour)
            evaluation['évaluateur'] = ligne[5]
            evaluations.append(evaluation)
        if TEST: print(evaluations)
    
    # Ensemble des eleves qui ont reçu une évaluation
    eleves  = []
    for e in evaluations:
        if e['élève'] not in eleves:
            eleves.append(e['élève']) 
 
    # Ensemble des formateurs ayant donné une évaluation             
    formateurs=[]
    for evaluation in evaluations:
        if evaluation['évaluateur'] not in formateurs:
            formateurs.append(evaluation['évaluateur'])
    
    ## Lecture des connaissances
    # Mise sous forme de graph orienté des connaissances présentent dans sql
    G = nx.DiGraph()
    with con:
        cur=con.cursor()
        cur.execute("select * from connaissances")
        connaissances_sql=cur.fetchall()
            
        #Creation des noeuds et des liens entre noeuds
        for connaissance in connaissances_sql:
            pere = connaissance[1]
            nom  = connaissance[2]
            if pere==None:
                G.add_node(nom)
            else:
                G.add_edge(pere, nom) # création des 2 noeuds et du lien 
    
    # Connaissance "feuille" 
    feuilles = []
    for noeud in G.nodes:
        if list(G.successors(noeud)) == []:
            feuilles += [noeud]
    
    # Dictionnaire des connaissances "feuille", associée à leurs catégories 
    # de connaissances par ordre d'importance croissante 
    connaissances = {}           
    for feuille in feuilles:
        connaissances[feuille] = []
        noeud = feuille
        while list(G.predecessors(noeud)) != [] :
            if noeud != feuille:
                connaissances[feuille] += [noeud]
                print (feuille, ',\t', noeud, ':', connaissances[feuille])
            noeud = list(G.predecessors(noeud))[0]
        connaissances[feuille] += [noeud]
        print (feuille, ',\t', noeud, ':', connaissances[feuille])
                     
    # On change l'ordre pour avoir un ordre décroissant 
    for feuille in connaissances.keys():
        connaissances[feuille] = tuple(reversed(connaissances[feuille]))
    
    # On retourne les données sous forme de liste de dictionnaires
    # connaissances est un dictionnaire de tuples
    # evaluations est une liste de dictionnaires
    donnees= eleves, formateurs, connaissances, evaluations
    
    # Affichage à la console des données: 
    if TEST:
        print(10*'-','EVALUATIONS',10*'-')
        print(evaluations)
        print(10*'-','CONNAISSANCES',10*'-')
        print(connaissances)
        print(10*'-','STATISTIQUES SUR LES DONNEES',10*'-')
        print("nombre de connaissances:", len(connaissances))
        print("nombre d'évaluation:", len(evaluations))
        print("nombre d'élèves:", len(eleves))
        print("nombre d'évaluateurs':", len(formateurs))
    
    return donnees
    

def manipulation_data(base, nom_table, valeurs='', colonnes='', conditions='',
                      connecteur='', mode=''):
    """ Fonction générique de manipulation des données dans la base de données
    SQL. 
    
    :Paramètres:
    valeurs: données à ajouter ou à modifier
    colonnes: nom des colonnes à modifier
    conditions: condition(s) pour sélectionner le rang à modifier
    connecteur: sous la forme 'AND' ou 'OR'  
    mode: mode d'action sur les données. On a le choix entre 'update',
    'insert', 'delete'
    
    :Exemple:
    >>> valeurs = [9, , 'Alexandre', '01-20-2020']
    >>> colonnes = ['note', 'evaluateur', 'date', 12, 3]
    >>> conditions  = ['evaluateur', 'Alexandre', 'id_evaluation', '1']
    >>> connecteur = 'OR' 
    >>> mode = 'update'
    
    Cela permet de former la requête suivante:
    UPDATE evaluation set 'note'=9, 'evaluateur'='Alexandre', date='01-20-2020'
    WHERE evaluateur = 'Alexandre" OR id_evaluation = 1 
    """
    con = lite.connect('test123.db')
    cur = con.cursor()
   
    
    if mode == 'insert':
        #permet de récupérer l'id_ du dernier rang de notre table pour
        #add +1 (presque) automatique
        df2 = pd.read_sql("SELECT * FROM "+ nom_table, con)
        col=[]
        for colonnes in df2.head(0) :
            col.append(colonnes)
        cur.execute("SELECT MAX(" + col[0] + ") FROM " +nom_table)
        fetch = cur.fetchone()
        id_auto = fetch[0]+1
        
        requete = [' INSERT INTO ', ' VALUES ']
        requete_sql = requete[0] + nom_table + requete[1] +'( ' + "'"
        requete_sql += str(id_auto) + "', " 
        for valeur in valeurs:
            requete_sql += "'" + valeur + "'," 
        #[:-1] retire la dernière valeur -> la dernière virgule
        requete_sql = requete_sql [:-1]
        requete_sql += ' )'
        if TEST: print(requete_sql)
        con.execute(requete_sql)
        con.commit()
        
    elif mode == 'update':
        requete = [' UPDATE ', ' SET ', ' WHERE ']
        #création d'un dictionnaire avec les listes colonnes et valeurs 
        #passées en argument
        colonne_valeur = dict(zip(colonnes, valeurs))
        #création d'un dict à partir des conditions
        dictconditions = {conditions[i]: conditions[i+1] 
                         for i in range(0, len(conditions), 2)}
        
        #début de la requête SQL
        requete_sql = requete[0] + nom_table + requete[1]
        for colonnes, valeurs in colonne_valeur.items():
            requete_sql += colonnes + "=" + "'" + valeurs + "', "
        requete_sql = requete_sql [:-2] + requete[2]
        if connecteur == 'and':
            for condition, valeur in dictconditions.items():
                requete_sql += condition + "=" + "'" + valeur + "' AND "
            requete_sql = requete_sql [:-4]
        elif connecteur == 'or': 
            for condition, valeur in dictconditions.items():
                requete_sql += condition + "=" + "'" + valeur + "' OR "
            requete_sql = requete_sql [:-3]
        if TEST: print(requete_sql)
        con.execute(requete_sql)
        con.commit()

    elif mode == 'delete':
        requete = [' DELETE FROM ',' WHERE ' ]
        dictconditions = { conditions[i]: conditions[i+1] 
                         for i in range(0, len(conditions), 2)}        
        requete_sql = requete[0] + nom_table + requete[1]
        if connecteur == 'and':
            for condition, valeur in dictconditions.items():
                requete_sql += condition + "=" + "'" + valeur + "' AND "
            requete_sql = requete_sql [:-4]
        elif connecteur == 'or': 
            for condition, valeur in dictconditions.items():
                requete_sql += condition + "=" + "'" + valeur + "' OR "
            requete_sql = requete_sql [:-3]
        if TEST: print(requete_sql)
        con.execute(requete_sql)
        con.commit()

def mail(nom_fichier, extension, destinataires, adresse_envoie, mot_passe, str_now):
    """ Envoie un mail à une liste de destinataire avec en copie la base de
    données sous forme d'un fichier au format arbitraire donné par 
    extension
        :param nom_fichier : copie de la base de données
        :param extension : extension du fichier de sauvegarde 
        :param destinataires: liste des adresses mail de destination
        :param adresse_envoie: adresse d'envoie
        :param mot_passe: mot de passe de l'adresse d'envoie
        
    :Example:
        >>> mail("bdd", ".xlsx", [vb@gmail.com, sb@gmail.com],
        "max.lamenace00007@gmail.com", "45gx-A13")
        
    """
    
    # liste comprenant les destinataires

    message = MIMEMultipart()
    # objet du message
    message['Subject'] = "Sauvegarde BDD"
    # connexion au serveur sortant (en précisant son nom et son port)
    serveur = smtplib.SMTP('smtp.gmail.com', 587)#mettre le port 587acceder au parametre de google gmail
    #et aller dans l'onglet transfert POP/IMAP et activer les IMAP  
    # spécification de la sécurisation
    serveur.starttls()   
    # authentification
    serveur.login("andy7640000@gmail.com","Meeotamp76400")#Opération à faire: envoyer un mail au responsable, envoyer une facture... pour automatiser le travail de l'équipe3°) Opération à faire: envoyer un mail au responsable, envoyer une facture... pour automatiser le travail de l'équipe","password")#mettre son email et son mot de passe   
    # définir le message a envoyer
    msgError = "Veuillez envoyer une facture"
    # trouve le chemin du fichier peut importe l'ordinateur
    try:
        filename = os.getcwd() + nom_fichier+str_now+extension
        #Overture de notre fichier
        piece = open(nom_fichier+str_now+extension, "rb")
        #Encodage de la pièce jointe en Base64
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((piece).read())
        encoders.encode_base64(part)
        #Entete du mail generee
        part.add_header('Content-Disposition', "piece; filename= %s" 
                        % filename)
        # Attache de la pièce jointe à l'objet "message"
        message.attach(part)
        #encodage du string en utf-8
        texte= message.as_string().encode('utf-8')
          ## Envoie du message
        serveur.sendmail("andy7640000@gmail.com","andy7640000@gmail.com","test")#mettre son mail
    except FileNotFoundError:
        serveur.sendmail("andy7640000@gmail.com ","andy7640000@gmail.com",msgError)#mettre son mail
mail("sauvegarde BDD","rb","andy7640000@gmail.com","andy7640000@gmail.com","Meeotamp76400","str_now")
#print(envoyer_un_mail
#appeler la fonction et mettres les paramètres qu'il faut (nom fichier,extension,mettre,
#deux fois son mail et son mot de passe)
def remove(extension, nbre_jours=1):
    """ Supprime tous les fichiers avec l'extension mis en paramètre dans 
    le répertoire de travail (là ou se trouve le programme!)"""
    # recupère la date actuelle et la met en microseconde
    date_actuelle = time.time()
    # variable qui recupere notre variable date_actuelle plus les nb jours 
    # qu'on souhaite'(ici 1 jour)
    # TODO: mettre le délai (cutoff) en paramètre de la fonction 
    cutoff = date_actuelle + nbre_jours * 86400
    # file recupere le repertoire positionné peut importe l'ordinateur'
    file = os.getcwd()
    # transforme le type de file (str du coup) en chemin
    p = Path(file)
    # boucle qui va lister tous les fichiers "extension"
    for xfile in list(p.glob('*'+extension)):
        print(xfile)
        # transforme notre xfile (fichier) en microseconde
        c = os.path.getctime((xfile))
        # si notre fichier (xfile) est antérieur a notre date 
        if c < cutoff:
            #supprime notre fichier
              os.remove(xfile) 
        
def sauvegarde_base_de_donnees():
    # listes des feuilles excel et des tables
    sheet=[]
    tb=[]
    
    # connexion db
    con = lite.connect('test123.db')
    cursorObj = con.cursor()
    
    # requête sql pour récupérer toutes les tables de la db
    cursorObj.execute('SELECT name from sqlite_master where type= "table"')
    #stocker dans tables la liste des tables dans .db
    tables=cursorObj.fetchall()
    
    # chaque table de la base de données est sauvegardée sous forme d'un 
    # fichier Excel  
    for table in tables:
        # Date actuelle 
        now = datetime.datetime.now()
        # date actuelle en chaine de carac
        str_now = now.strftime("%Y-%m-%d-%H-%M-%S")
        with con:
            # onglet prend la valeur d'index 0 du tuple (le nom des tables)
            onglet = table[0]
            # on ajoute à la feuille excel la valeur de "onglet", 1ere itération
            # onglet : connaissances
            sheet.append(onglet)

            # on récupére toutes les données de "onglet"
            requete_SQL = "SELECT * FROM " +str(onglet)
            table_tb = pd.read_sql(requete_SQL, con)
            #on ajoute à la liste tb[] 
            tb.append(table_tb)
        nom_fichier = "BDD"+ str_now +".xlsx"

    with pd.ExcelWriter(nom_fichier) as writer:
        for i in range (0,len(sheet)) :
            tb[i].to_excel(writer, sheet_name=sheet[i], index=False )
        print(table_tb.head())
        print("C'est sauvegardé à "+str_now+" dans "+nom_fichier)

    # Envoie des mails et des fichiers de sauvegarde 
    mail('BDD','.xlsx')
    
    # Supprime tous les fichiers Excel, qui sont sencés être des copies de la 
    # base de données, lorsqu'ils sont plus vieux que 1 jour.   
    remove('.xlsx', nbre_jours=1)

    
### Tests 
if __name__ == '__main__':    
    # ACTIONS DU PROGRAMME PRINCIPAL
    initialise_les_donnees = False
    test_robot_sauvegarde = False
    test_import = False
    manip_data = False
    sauvegarde = False
    generation = False
    
    if initialise_les_donnees:
        initialisation_base_de_donnees()
        
    if test_robot_sauvegarde:        
        debut = datetime.datetime.now()        
        s = sched.scheduler()
        while datetime.datetime.now() < debut + timedelta(seconds=5):
            s.enter(2, 1, sauvegarde_base_de_donnees)
            s.run() 
            
    if test_import:
        fichier = 'BDD2020-04-17-14-02-10.xlsx'
        initialisation_base_de_donnees(mode='rechargement', fichier=fichier)
                                    
    if manip_data:
        #'valeurs' et 'colonnes' doivent être mise au format ['valeurs'] 
        #ou bien ['valeurs1', 'valeurs2'] quand passés en argument de fonction
        manipulation_data('test123.db', 'evaluations', 
                          valeurs=['12'], colonnes=['note'],
                          conditions=['evaluateur', 'Alexandre', 'id_evaluation', '1'], connecteur='and',
                          mode='delete')

    if sauvegarde:
        sauvegarde_base_de_donnees()
        
    if generation :
        generation_donnees()
        
        